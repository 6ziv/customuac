<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>UAC Control</source>
        <translation>UAC控制台</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="27"/>
        <source>Basic</source>
        <translation>基本</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="39"/>
        <source>Theme:</source>
        <translation>主题：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="62"/>
        <location filename="../mainwindow.ui" line="143"/>
        <location filename="../mainwindow.ui" line="260"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="75"/>
        <source>Import theme from file</source>
        <translation>从文件导入主题</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="88"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="102"/>
        <source>Advanced</source>
        <translation>进阶</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="114"/>
        <source>Allow Network</source>
        <translation>允许访问网络</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="127"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Though it&apos;s not allowed for themes to access local files (at least I&apos;ve tried to do this), themes can declare a &amp;quot;local storage&amp;quot; to save data.&lt;/p&gt;&lt;p&gt;Different themes should have seperate local storages, but that&apos;s not promised.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;主题原则上不应该直接访问本地文件，而且我也试图阻止这样的行为,但是它们可以声明一个本地存储对象以存储一些数据。&lt;/p&gt;&lt;p&gt;不同的主题的本地存储之间应该互相独立，但是这一点并不被保证。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="130"/>
        <source>Allow local storage</source>
        <translation>允许本地存储</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="156"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This program replaces one of the system files, so &amp;quot;sfc.exe&amp;quot; may try to fix this &amp;quot;corruption&amp;quot; if you run it.&lt;/p&gt;&lt;p&gt;By registering a service, we can lock the replaced file so sfc won&apos;t be able to perform the &amp;quot;fix&amp;quot;. What&apos;s more, the service will check the file at startup, and try to ensure the replacement if needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;本程序会替换某些系统文件，因此，当你运行sfc.exe时，它可能会试图修复这些“受损”的文件。通过注册为服务，我们可以锁定这些被替换的文件，从而使得sfc无法“修复”它们。更进一步地，服务会在开机时检查这些文件，并在需要时试图重新进行替换。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="159"/>
        <source>Register service</source>
        <translation>注册为服务</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>allowlist</source>
        <translation>允许名单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="195"/>
        <source> Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="209"/>
        <source>Prompt on Secure Desktop</source>
        <translation>在安全桌面上运行</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="221"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="234"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="247"/>
        <source>Respect System Settings</source>
        <translation>应用系统设定</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="283"/>
        <source>Select a theme file</source>
        <translation>选择主题文件</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="285"/>
        <source>RCC files</source>
        <translation>RCC文件</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="285"/>
        <source>All files</source>
        <translation>所有文件</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="291"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="292"/>
        <source>The seected file is not a valid qt resource bundle.</source>
        <translation>所选择的文件不是有效的Qt资源文件</translation>
    </message>
</context>
<context>
    <name>QTranslator</name>
    <message>
        <location filename="../mainwindow.cpp" line="108"/>
        <source>default theme</source>
        <translation>默认主题</translation>
    </message>
</context>
</TS>
