#pragma once
#include <Windows.h>
#include <string>
#include <vector>
namespace Utils {
	constexpr std::wstring_view CommandTrustedInstaller = L"TrustedInstaller";

	enum PROCESS_CMD {
		CMD_NONE = 0,
		CMD_TRUSTEDINSTALLER
	};

	PROCESS_CMD GetRunCmd() {
		std::wstring cmd = GetCommandLineW();
		if (cmd == CommandTrustedInstaller)return CMD_TRUSTEDINSTALLER;
		return CMD_NONE;
	}

	std::wstring GetCurrentProcessPath() {
		std::vector<wchar_t> path;
		DWORD len, retLen;
		len = MAX_PATH;
		do {
			path.resize(len);
			retLen = GetModuleFileNameW(NULL, path.data(), len);
		} while (retLen == len && GetLastError() == ERROR_INSUFFICIENT_BUFFER);
		return path.data();
	}

	std::wstring GetSystemPath() {
		size_t size = GetSystemDirectoryW(NULL, 0);
		std::vector<wchar_t> buffer;
		buffer.resize(size);
		GetSystemDirectoryW(buffer.data(), (UINT)(size));
		std::wstring ret = buffer.data();
		buffer.clear();
		if (ret.back() == L'\\')
			ret.pop_back();
		return ret;
	}
};