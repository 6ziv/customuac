#include <Windows.h>
#include <string>
#include <functional>
#include <boost/scope_exit.hpp>
#include <boost/range/algorithm/equal.hpp>
namespace Utils {
	bool file_is_equal(const std::wstring& file1, const std::wstring file2, std::function<void(void)> callback) {
		HANDLE hFile1 = CreateFileW(file1.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile1 == INVALID_HANDLE_VALUE)
			return false;
		std::invoke(callback);
		BOOST_SCOPE_EXIT(hFile1) { CloseHandle(hFile1); } BOOST_SCOPE_EXIT_END;

		HANDLE hFile2 = CreateFileW(file2.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile2 == INVALID_HANDLE_VALUE)
			return false;
		std::invoke(callback);
		BOOST_SCOPE_EXIT(hFile2) { CloseHandle(hFile2); } BOOST_SCOPE_EXIT_END;

		LARGE_INTEGER fileSize1;
		if (!GetFileSizeEx(hFile1, &fileSize1))return false;
		std::invoke(callback);

		LARGE_INTEGER fileSize2;
		if (!GetFileSizeEx(hFile2, &fileSize2))return false;
		std::invoke(callback);

		if (fileSize1.QuadPart != fileSize2.QuadPart)return false;

		HANDLE mapping1 = CreateFileMappingW(hFile1, NULL, PAGE_READONLY, 0, 0, NULL);
		if (mapping1 == NULL)
			return false;
		std::invoke(callback);
		BOOST_SCOPE_EXIT(mapping1) { CloseHandle(mapping1); } BOOST_SCOPE_EXIT_END;

		HANDLE mapping2 = CreateFileMappingW(hFile2, NULL, PAGE_READONLY, 0, 0, NULL);
		if (mapping2 == NULL)
			return false;
		std::invoke(callback);
		BOOST_SCOPE_EXIT(mapping2) { CloseHandle(mapping2); } BOOST_SCOPE_EXIT_END;

		unsigned char* content1 = reinterpret_cast<unsigned char*>(MapViewOfFile(mapping1, FILE_MAP_READ, 0, 0, 0));
		if (content1 == NULL)
			return false;
		std::invoke(callback);
		BOOST_SCOPE_EXIT(content1) { UnmapViewOfFile(content1); } BOOST_SCOPE_EXIT_END;

		unsigned char* content2 = reinterpret_cast<unsigned char*>(MapViewOfFile(mapping2, FILE_MAP_READ, 0, 0, 0));
		if (content2 == NULL)
			return false;
		std::invoke(callback);
		BOOST_SCOPE_EXIT(content2) { UnmapViewOfFile(content2); } BOOST_SCOPE_EXIT_END;

		return std::equal(content1, content1 + fileSize1.QuadPart, content2);
	}
}