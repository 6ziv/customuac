<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="23"/>
        <source>UAC</source>
        <translation>用戶賬戶控製</translation>
    </message>
    <message>
        <location filename="main.qml" line="62"/>
        <source>Are you sure to %1 %2 %3 %4, which is located at %5?</source>
        <translation>您确定要%1%4%2 %3 吗？
其位于路径%5。</translation>
    </message>
    <message>
        <location filename="main.qml" line="63"/>
        <source>load</source>
        <translation>加載</translation>
    </message>
    <message>
        <location filename="main.qml" line="63"/>
        <source>run</source>
        <translation>運行</translation>
    </message>
    <message>
        <location filename="main.qml" line="64"/>
        <source>dynamic library</source>
        <translation>動態鏈接庫</translation>
    </message>
    <message>
        <location filename="main.qml" line="64"/>
        <source>application</source>
        <translation>應用程式</translation>
    </message>
    <message>
        <location filename="main.qml" line="66"/>
        <source>signed by %1</source>
        <translation>由 %1 簽名的</translation>
    </message>
    <message>
        <location filename="main.qml" line="102"/>
        <source>Username:</source>
        <translation>本機賬戶：</translation>
    </message>
    <message>
        <location filename="main.qml" line="123"/>
        <source>Password:</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="main.qml" line="131"/>
        <source>Input password</source>
        <translation>請輸入密碼</translation>
    </message>
    <message>
        <location filename="main.qml" line="141"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="main.qml" line="163"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="main.qml" line="180"/>
        <source>Add to allow list</source>
        <translation>添加至允許列表</translation>
    </message>
</context>
</TS>
